const fs  = require('fs');
const elasticbulk = require('elasticbulk');

// var data = csv.fromPath('data.csv');
const fetch = require("node-fetch");

function csvJSON(csv){

    var lines=csv.split("\n");

    var result = [];
    var headers=lines[0].split(",");

    for(var i=1;i<lines.length;i++){

        var obj = {};
        var currentline=lines[i].split(",");

        for(var j=0;j<headers.length;j++){
            obj[headers[j]] = currentline[j];
        }

        result.push(JSON.stringify(obj));

    }

    return result; 
}

var data = csvJSON(fs.readFileSync("./Data.csv").toString());

elasticbulk.import(data, {
    index: 'blogs',
    type: 'TP',
    host: 'http://localhost:9200'
}).then(function(res) {
    console.log(res);
})
